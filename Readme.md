# Instruction

Build with
 `make`

Run with
 `make test`

Debug with
 `qemu-system-x86_64 -boot a -fda  mbr -s -S`
 then
 `gdb`
 ```
  target remote localhost:1234
  layout asm
  b *0x7c00
  c
  ```

# ASM reg

https://en.wikibooks.org/wiki/X86_Assembly/X86_Architecture

## In 16 bits
There is 4 main 16bits reg: AX, BX, CX, DX.
You can access the 8 MSB from AX with AH and the 8 LSB with AL.( BH,BL, CH, CL)

SI and DI are 16bits index register often used for pointer but can contains data.

SP is stack pointer, BP is base pointer

AX(AH, AL), BX(BH, BL), CX(CH, CL), DX(DH, DL), SI, DI, SP, BP

## In 32bits
Main register are "Extended" to 32bits but 16bits of 8bits direct access is not possible anymore.

EAX, EBX, ECX, EDX, ESI, EDI, ESP, EBP

## In 64bits
Prefixed by R
RAX, RBX, RCX, RDX, RSI, RDI, RSP, RBP


# Bios Memory
Address range            | Description
----------------------- | -----------------------
0x0     -   0x03FF      |IVT
0x400   -   0x4FF       |BIOS Data
0x500   -   0x7BFF      |Unused
0x7C00  -   0x7D77      |Bootloader
0x7E00  -   0x9FFFF     |Unused
0xA0000 -   0xBFFFF     |Video memory
0xB0000 -   0xB7777     |Video memory monochrome
0xB8000 -   0xBFFFF     |Video memory color
0xC0000 -   0xC7FFF     |BIOS ROM
0x80000 -   0xEFFFF     |BIOS shadow memory
0xF0000 -   0xFFFFF     |BIOS System


# Interrupt Vector Table
INT_NUM     |   Short Description PM
----------- | ----------------------
0x00        |  Division by zero
0x01        |  Debugger
0x02        |  NMI
0x03        |  Breakpoint
0x04        |  Overflow
0x05        |  Bounds
0x06        |  Invalid Opcode
0x07        |  Coprocessor not available
0x08        |  Double fault
0x09        |  Coprocessor Segment Overrun (386 or earlier only)
0x0A        |  Invalid Task State Segment
0x0B        |  Segment not present
0x0C        |  Stack Fault
0x0D        |  General protection fault
0x0E        |  Page fault
0x0F        |  reserved
0x10        |  Math Fault
0x11        |  Alignment Check
0x12        |  Machine Check
0x13        |  SIMD Floating-Point Exception
0x14        |  Virtualisation Exception
0x15        |  Control Protection Exception

# BIOS IRQ
https://en.wikipedia.org/wiki/BIOS_interrupt_call
