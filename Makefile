# Used to generated .d file that deal with header dependencies
CPPFLAGS = -MMD
AS=nasm
ASFLAGS += -f elf32
LDFLAGS += -m32 -nostdlib -static -fno-common -fno-use-cxa-atexit -fno-exceptions -fno-non-call-exceptions -fno-weak -fno-rtti -fno-stack-protector
CFLAGS += -m32 -Wall -Wextra -Werror -ffreestanding  -fno-exceptions -fno-pie -fno-stack-protector
CXXFLAGS += -m32 -Wall -Wextra -Werror -ffreestanding  -fno-exceptions -fno-rtti -fno-pie

SUBDIRS := core drivers

CPPFLAGS += $(foreach dir, $(SUBDIRS), -I$(dir))

asmsrc=$(wildcard *.asm)
asmobj=$(asmsrc:%.asm=%.o)
csrc=$(shell find $(SUBDIRS) -type f -name "*.c")#   $(wildcard *.c)
cobj=$(csrc:%.c=%.o)
deps = $(csrc:%.c=%.d)

kernel:$(asmobj) $(cobj) linker.ld
	$(CXX) $(LDFLAGS) $(cobj) $(asmobj) -o $@ -T linker.ld

fd.img: kernel
	dd if=/dev/zero of=$@ bs=512 count=2880
	dd if=$< of=$@ conv=notrunc

#https://gcc.gnu.org/onlinedocs/gcc/x86-Function-Attributes.html#x86-Function-Attributes
core/exception_handler.o:core/exception_handler.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -mgeneral-regs-only  -c $< -o $@
core/irq_handler.o:core/irq_handler.c
	$(CC) $(CPPFLAGS) $(CFLAGS) -mgeneral-regs-only  -c $< -o $@

%.o:%.asm
	$(AS) $(ASFLAGS) -o $@ $<

test:kernel
	qemu-system-x86_64 -fda $<

clean:
	$(RM) kernel $(asmobj) $(cobj) $(deps)

ifneq ($(MAKECMDGOALS),clean)
-include $(deps)
endif
