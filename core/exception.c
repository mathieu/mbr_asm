#include "exception.h"
#include "idt.h"
#include "interrupt.h"
#include "irq.h"

exception_handler exception_handler_array[EXCEPTION_NUM] = {
    NULL,
};

int exceptionSetRoutine(int exception, exception_handler handler)
{
	uint32_t flags;
	if ((exception < 0) || exception >= EXCEPTION_NUM)
		return -1;

	disable_IRQs(flags);

	exception_handler_array[exception] = handler;

	idt_set_handler(EXCEPTION_INTERRUPT_BASE_ADDRESS + exception, (unsigned int)handler,
			0);
	restore_IRQs(flags);
        return 0;
}
