#pragma once
#include "types.h"

#define IDT_NUM 256

#define RING_0 0
#define RING_1 1
#define RING_2 2
#define RING_3 3

#define SEGMENT_IDX_NULL 0
#define SEGMENT_IDX_CODE 1
#define SEGMENT_IDX_DATA 2

struct idtEntry {
	uint16_t offset_low;
	uint16_t seg_sel;
	uint8_t reserved : 5;
	uint8_t flags : 3;
	uint8_t type : 3;
	uint8_t op_size : 1;
	uint8_t zero : 1;
	uint8_t dpl : 2;
	uint8_t present : 1;
	uint16_t offset_high;
} __attribute__((packed));

/**
 * The IDT register, which stores the address and size of the
 * IDT.
 *
 * @see Intel x86 doc vol 3, section 2.4, figure 2-4
 */
struct idtRegister {
	uint16_t limit;
	uint32_t base_addr;
} __attribute__((packed, aligned(8)));

/* Build segment http://wiki.osdev.org/Selector*/
#define BUILD_SEGMENT_SELECTOR(desc_privilege, in_ldt, index)                  \
	((((desc_privilege)&0x3) << 0) | (((in_ldt) ? 1 : 0) << 2) |           \
	 ((index) << 3))

int idtSetup();
int idt_set_handler(int index, unsigned int addr, int priviledge);
