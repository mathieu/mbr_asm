#pragma once
#include "types.h"

void *memcpy(void *dest, const void *src, size_t n );
