#include "klibc.h"

void *memcpy(void *dst, const void *src, size_t n)
{
        char *dstChar       = dst;
        const char *srcChar = src;
        for (size_t i = 0; i < n; i++) {
                *(dstChar++) = *(srcChar++);
        }
        return dst;
}
