#include "exception.h"
#include "vga.h"

// Need GCC > 6
__attribute__((interrupt)) void print_handler(struct interrupt_frame *frame, ulong error_code)
{

        printStringDetails("EXCEPTION", RED, BLACK, 0, VGA_HEIGHT - 1);
        printIntDetails(error_code, RED, BLACK, 11, VGA_HEIGHT - 1);
        (void)frame;
        (void)error_code;
}
