#pragma once
#include "types.h"

struct interrupt_frame;

//Exception
void print_handler(struct interrupt_frame *frame, ulong error_code);

//IRQ
void keyboard_handler(struct interrupt_frame *frame);
void timer_handler(struct interrupt_frame *frame);
