#include "interrupt.h"
#include "io.h"
#include "irq.h"
#include "keyboard.h"
#include "pic.h"
#include "vga.h"

// Need GCC > 6
__attribute__((interrupt)) void keyboard_handler(struct interrupt_frame *frame)
{
        EOIIrq(IRQ_KEYBOARD);
        keyboard_do_irq();
        (void)frame;
}

__attribute__((interrupt)) void timer_handler(struct interrupt_frame *frame)
{
        static int timeCnt = 0;
        EOIIrq(IRQ_TIMER);
        printIntDetails(timeCnt++, RED, BLACK, 20, VGA_HEIGHT - 1);
        (void)frame;
}
