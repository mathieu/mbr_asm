#include "exception.h"
#include "gdt.h"
#include "idt.h"
#include "interrupt.h"
#include "io.h"
#include "irq.h"
#include "pit.h"
#include "types.h"
#include "vga.h"

void cpuid(int code, uint32_t *a, uint32_t *d)
{
    asm volatile("cpuid" : "=a"(*a), "=d"(*d) : "0"(code) : "ebx", "ecx");
}

void kmain()
{
    initVGA(BLACK, GREEN);
    printString("Setting up IDT\n");
    gdtSetup();
    idtSetup();
    irqSetup();
    initPit(100);

    printString("Setting up IRQ handlers\n");
    irqSetRoutine(IRQ_KEYBOARD, keyboard_handler);
    irqSetRoutine(IRQ_TIMER, timer_handler);
    printString("Enabling HW interrupts\n");
    exceptionSetRoutine(EXCEPTION_DOUBLE_FAULT, print_handler);
    // Enabling the HW interrupts
    asm volatile("sti\n");
    int count = 0;
    while (1) {
        printIntDetails(count++, GREEN, BLACK, 0, VGA_HEIGHT - 1);
    }
    printString("exiting\n");
}
