#include "irq.h"
#include "idt.h"
#include "pic.h"
#include "types.h"

int irqSetup()
{
	initPic();
	return 0;
}

irq_handler irq_handler_array[IRQ_NUM] = {NULL, };

int irqSetRoutine(int irq, irq_handler handler)
{
    uint32_t flags;
    if ((irq < 0) || irq >= IRQ_NUM)
        return -1;

    disable_IRQs(flags);

    irq_handler_array[irq] = handler;

    if (handler != NULL) {
        int ret =
            idt_set_handler(IRQ_INTERRUPT_BASE_ADDRESS + irq, (unsigned int)irq_handler_array[irq], 0);
        if (!ret)
            enableIrq(irq);
    }
    restore_IRQs(flags);
    return 0;
}
