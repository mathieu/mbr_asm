#pragma once
#include "types.h"

// https://wiki.osdev.org/Text_UI
#define BLACK   0x00
#define BLUE    0x01
#define GREEN   0x02
#define CYAN    0x03
#define RED     0x04
#define MAGENTA 0x05
#define BROWN   0x06
#define GREY    0x07
#define WHITE   0x0F

#define VGA_ADDR   0xB8000
#define VGA_WIDTH  80
#define VGA_HEIGHT 25

int initVGA(uint bgColor, uint color);
void clearScreen(uint bgColor);
void printInt(int integer);
void printIntDetails(int integer, uint color, uint bgColor, int startX, int startY);
void printCharDetails(char str, uint color, uint bgColor, int startX, int startY);
void printStringDetails(const char *str, uint color, uint bgColor, int startX, int startY);
void printString(const char *str);
void printChar(const char str);
void vgaScrollUp(void);
