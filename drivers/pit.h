#pragma once

// C.f https://wiki.osdev.org/PIT

#define PIT_FREQ 1193182
#define PIT_CHAN_0 0x40 // IRQ0
#define PIT_CHAN_1 0x41 // Used for DRAM refresh. Not used anymore
#define PIT_CHAN_2 0x42 // PC Speaker
#define PIT_CMD 0x43
// Cmd are
// 7-6: select channel. 0 ->chan0, 1 -> chan1, 2 -> chan 2, 3 -> read back
// 5-4: access mode. 0 -> latch count; 1 -> low value only; 2 -> high value only;
// 3 -> low then high 3-1: mode. See https://wiki.osdev.org/PIT

int initPit(unsigned int freq);

