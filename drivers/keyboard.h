#pragma once

// Pushing generate MAKE_CODE (<0x80)
// Releasing generate BREAK_CODE (break_code = make_code + 0x80)
#define BREAK_CODE 0x80
void keyboard_do_irq();
