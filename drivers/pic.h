#pragma once
// 2 PIC 8259 are available on x86
//
//    Master - command: 0x20, data: 0x21
//    Slave - command: 0xA0, data: 0xA1
//
// http://www.jamesmolloy.co.uk/tutorial_html/5.-IRQs%20and%20the%20PIT.html
// SimpleOS art2
// http://wiki.osdev.org/PIC

#define PIC_MASTER_CMD 0x20
#define PIC_SLAVE_CMD 0xa0
#define PIC_MASTER_DATA 0x21
#define PIC_SLAVE_DATA 0xa1
#define PIC_EOI 0x20 /* End-of-interrupt command code */


void EOIIrq(int irq);
void initPic(void);
void enableIrq(int irq);
void disableIrq(int irq);
