#include "pit.h"
#include "io.h"

int initPit(unsigned int freq)
{
	unsigned int divisor = PIT_FREQ / freq;
	if (divisor > 65535)
		divisor = 0; // Used to represent 35536
	outb(PIT_CMD, 0x34); // chan 0; low then high; mode 2
	outb(PIT_CHAN_0, divisor & 0xFF);
	outb(PIT_CHAN_0, divisor >> 8u);

	return 0;
}
